# Legality of ROM extraction 

**You must not rely on this for legal advice; if you need legal advice, talk to a lawyer.**

In researching an arcade emulation feature for The MagPi magazine, we encountered a question of whether UK copyright law allowed users to extract PAK files from Capcom Arcade Stadium in order to use the compressed ROM images with a vanilla version of MAME. Neil Brown of [decoded.legal](https://decoded.legal/) has offered an opinon on this, concluding that it is not.

Our correspondence is reproduced below:


KG:
>>>
Capcom Arcade Stadium, which can be bought on the Steam digital storefront, uses a repacked version of the open source MAME emulator and includes compressed MAME-compatible arcade ROM images, compressed into .pak format. These can be extracted using the QuickBMS extractor with a script to identify the file and directory structures. The extracted ROMs require no further action beyond zipping them to allow them to be run in MAME on a Raspberry Pi.

Clause 5 of the [Capcom Arcade Stadium EULA](https://store.steampowered.com//eula/1515950_eula_1) states that "Any act which may infringe the intellectual property rights of Capcom such as to copy (except for the purpose of installing the Program), modify, alter, translate reverse engineer, decompile, disassemble, extract or otherwise attempt to discover the source code of the Program or any part thereof, except and only to the extent that this activity is expressly permitted by the law of the user’s country of residence."

This appears to bounce the question over to [UK copyright law](https://www.legislation.gov.uk/ukpga/1988/48/part/I/chapter/III/crossheading/computer-programs-lawful-users). The law is explicit about decompilation of source code, but provides less specific guidance on extracting a compressed archive.

Bearing that it mind, it looks as though it may be permitted under F6 50C (1), stating that

"It is not an infringement of copyright for a lawful user of a copy of a computer program to copy or adapt it, provided that the copying or adapting—
(a)is necessary for his lawful use; and
(b)is not prohibited under any term or condition of an agreement regulating the circumstances in which his use is lawful."

However, (b) appears to refer us back to the original EULA.

While ROM extraction is a fascinating technical exercise that produces entertaining results, we're very keen for our readers to stay within the bounds of UK law.

Am I correct in thinking that the EULA clause forbidding extraction takes precedence over F6 50C's clause allowing adaptation for lawful use?
>>>


Neil:
>>>
Assuming that Capcom owns the copyright in the ROMs, my view is that, when even a legitimate Steam purchaser extracts the ROMs and runs them on their own Raspberry Pi, they infringe Capcom’s copyright. There is no general right of “personal use” under English copyright and, while we did have that right for a (very short) while, it expressly excluded computer programs.

s50C does not, in my view, assist the user in this situation.

First, I am sceptical that they are a “lawful user” if they do anything other than play the game via Steam. There’s scope for argument over that, but I think it’s doubtful. 

Second, I very much doubt that extracting the ROMs and popping them onto their RPi is “necessary” for their lawful use. If they needed to make a tweak to get the game to run properly via Steam, then I think they’d be on reasonably safe ground, but I think they’d struggle to persuade a judge that their use was “necessary”.

In support of this, I look at Article 5(1) and recital 13 to the [computer programs directive](https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32009L0024&from=EN), from which s50C (via some 1992 regulations) derives. Recital 13 says:

"The exclusive rights of the author to prevent the unauthorised reproduction of his work should be subject to a limited exception in the case of a computer program to allow the reproduction **technically necessary for the use of that program by the lawful acquirer. This means that the acts of loading and running necessary for the use of a copy of a program which has been lawfully acquired, and the act of correction of its errors**, may not be prohibited by contract. In the absence of specific contractual provisions, including when a copy of the program has been sold, any other act necessary for the use of the copy of a program may be performed in accordance with its intended purpose by a lawful acquirer of that copy.”

Similarly, Article 5(1) says that the general right (as opposed to the specific rights of (5(2), 5(3), and 6) applies only "in the absence of specific contractual provisions”.

There’s always scope for argument when it comes to the interpretation of law, but I don’t think there is much merit in the argument one would need to make to claim that the acts here were consistent with English copyright law.

In addition to the likelihood that doing this was an infringement (because the copyright holder has the exclusive right to authorise copying), there’s also the possibility that the method of extraction breaches [s296ZA](https://www.legislation.gov.uk/ukpga/1988/48/section/296ZA) which deals with circumvention of technological measures. Whether that would stick would depend on quite what the script was doing, and whether it is needed because of an attempt by the rights holder to prevent unauthorised acts.

Of course, one might question whether Capcom is likely to pursue an individual user who does this solely for their own use. Unless, perhaps, they, license ROMs for the Pi at a higher price? But, even so, the incentive to bring a claim seems low. If someone were distributing the ROMs, then that might be a different matter entirely.
>>>

